import cn from 'classnames';
import Link from 'next/link';
import { sharedRoutes } from '../config';

export const FullPage = ({ children }) => (
  // <div className="h-screen bg-darkPage overflow-hidden flex flex-col">
  <div className="h-screen bg-darkPage overflow-hidden flex flex-col">
    {children}
  </div>
);

FullPage.WithHeader = ({ children }) => (
  <FullPage>
    <Header />
    <div className="h-full overflow-y-auto">
      {children}
    </div>
  </FullPage>
);

const NavItem = ({ children, href }) => {
  const active = false;
  return (
    <Link href={href}>
      <a className={cn('text-xl cursor-pointer transform hover:font-medium', {
        'bg-gradient-to-r from-green-300 to-red-200 text-transparent bg-clip-text font-semibold': active,
        'text-gray-300': !active,
      })}
      >
        {children}
      </a>
    </Link>
  );
};

export const Header = () => (
  // <div className="h-16 bg-[#26333b] flex flex-row items-center px-8 justify-between rounded-md">
  <div className="h-16 bg-darkHeader flex flex-row items-center px-8 justify-between rounded-md">
    {/*<div className="text-3xl font-logo bg-gradient-to-r bg-clip-text text-transparent from-green-300 to-red-300">nxtcoder17</div>*/}
    <div className="text-3xl font-logo bg-gradient-to-r bg-clip-text text-transparent from-gray-600 to-gray-400">nxtcoder17</div>

    {/* <div className="flex flex-row gap-16 items-center"> */}
    {/*   <NavItem href={sharedRoutes.HOME}>Home</NavItem> */}
    {/*   <NavItem href={sharedRoutes.BLOG}>Blog</NavItem> */}
    {/*   <NavItem href={sharedRoutes.CONTACT}>Contact</NavItem> */}
    {/* </div> */}
  </div>
);
