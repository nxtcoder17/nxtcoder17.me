import Link from 'next/link';
import { FullPage } from 'shared/layouts/page';
import fsPromise from 'fs/promises';
import blogConfig from '../blogs.config';

const Item = ({
  title, date, shortDescription, tags, href,
}) => (
  <Link href={href}>
    <a>
      <div className="flex flex-col gap-3 bg-darkHeader text-textPrimary px-8 py-3 text-gray-300 rounded-2xl cursor-pointer">
        <div>
          <div className="text-2xl font-bold tracking-wide line-clamp-2 bg-gradient-to-r from-gray-500 to-gray-400 bg-clip-text w-max">
            {title}
          </div>
          <div className="line-clamp-2 text-lg tracking-wide text-textPrimary">{shortDescription}</div>
        </div>
        <div className="flex justify-between">
          <div className="flex flex-row gap-2 italic tracking-wider font-medium">
            { tags.map((tag, idx) => (
              <div key={idx} className="text-textPrimary hover:text-gray-400 cursor-pointer gap-1">
                <span className="text-xs">#</span>
                {tag}
              </div>
            )) }
          </div>
          <div className="font-medium tracking-wider">
            {date}
          </div>
        </div>
      </div>
    </a>
  </Link>
);

const Page = ({ blogsList }) => (
  <FullPage.WithHeader>
    <div className="container mx-auto flex flex-col gap-8 py-8">
      {blogsList.map((blog, idx) => (
        <Item key={idx}
          title={blog.title}
          date={blog.date}
          tags={blog.tags}
          href={blog.href}
          shortDescription={blog.shortDescription}
        />
      ))}
    </div>
  </FullPage.WithHeader>
);

export default Page;

export async function getStaticProps() {
  const data = await fsPromise.readFile(blogConfig.metadataMap);
  const metaMap = JSON.parse(data);

  const blogsList = Object.keys(metaMap).map((item) => ({
    href: `/p/${item}`,
    title: metaMap[item].title,
    tags: metaMap[item].tags,
    date: metaMap[item].date,
    shortDescription: metaMap[item].description,
  }));

  return {
    props: {
      blogsList,
    },
  };
}
