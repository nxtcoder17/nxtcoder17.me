import fsPromise from 'fs/promises';
import blogConfig from '../../blogs.config';
import fm from 'front-matter';
import { serialize } from 'next-mdx-remote/serialize';
import { MDXRemote } from 'next-mdx-remote';
import { MDXComponents } from '../../lib/mdx';
import { FullPage } from 'shared/layouts/page';

const Page = ({ mdxSource }) => (
  <FullPage.WithHeader>
    <div className="container mx-auto p-4 flex flex-col gap-4">
      <MDXRemote {...mdxSource} components={MDXComponents} />
    </div>
  </FullPage.WithHeader>
);

export async function getStaticPaths() {
  const data = await fsPromise.readFile(blogConfig.metaDataIdList);
  const blogIdList = JSON.parse(data.toString());
  return {
    paths: blogIdList.map((blogId) => ({ params: { blog_id: blogId } })),
    fallback: false,
  };
}

export async function getStaticProps(appCtx) {
  const { blog_id: blogId } = appCtx.params;
  const data = await fsPromise.readFile(blogConfig.metadataMap);
  const metaMap = JSON.parse(data.toString());
  const blogContent = await fsPromise.readFile(metaMap[blogId].path);
  const { body } = fm(blogContent.toString());
  const mdxSource = await serialize(body);
  return {
    props: {
      mdxSource,
    },
  };
}

export default Page;
