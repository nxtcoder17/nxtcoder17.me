const withTM = require('next-transpile-modules')(['shared']);

module.exports = withTM({
  httpAgentOptions: {
    keepAlive: false,
  },
  productionBrowserSourceMaps: true,
  experimental: {
    modern: true,
    asset: true,
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  webpack: (config) => config,
});
