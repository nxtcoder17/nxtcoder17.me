import path from 'path';

// assuming process.cwd() to be __dirname ie. root of the project
const indexDir = path.resolve(process.cwd(), './lib/_index');

const blogConfig = {
  blogsDir: process.env.BLOGS_DIR,
  indexDir,
  metadataMap: path.resolve(indexDir, 'meta-map.json'),
  metaDataIdList: path.resolve(indexDir, 'meta-ids.json'),
};

export default blogConfig;
