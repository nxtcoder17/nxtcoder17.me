const path = require('path');

const purgePath = [
  path.resolve(__dirname, './pages/**/*.{js,jsx}'),
  path.resolve(__dirname, './lib/**/*.{js,jsx}'),
  path.resolve(__dirname, '../../shared/layouts/**/*.{jsx,js}'),
  path.resolve(__dirname, '../../shared/styles/**/*.{jsx,js}'),
];

const blogTheme = {
  fontFamily: {
    sans: ['Recursive'],
    logo: ['Nova Mono'],
  },
  extend: {
    colors: {
      darkPage: '#111a1f',
      darkHeader: '#1a2833',
      blogPost: '#2b3740',
      textPrimary: '#9dafbd',
      code: {
        inline: '#263a4a',
      },
    },
  },
};

module.exports = {
  theme: blogTheme,
  // eslint-disable-next-line global-require
  presets: [require('../../tailwind.config')],
  content: purgePath,
};
