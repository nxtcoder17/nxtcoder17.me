import CodeBlock from './codeblock';
import { IoIosDisc } from 'react-icons/io';

// const Ul = (props) => <ul className="list-none list-inside px-4 text-gray-300" {...props} />;
const Ul = (props) => {
  const { children, ...rest } = props;
  return (
    <ul className="list-none list-inside px-4 text-gray-500" {...rest}>
      {children instanceof Array && children.filter(child => typeof child != "string").map(
        // eslint-disable-next-line react/no-array-index-key
        (child, idx) => <Ul.Li key={idx} {...child.props} />,
      )}
      {!(children instanceof Array) && <Ul.Li>{children}</Ul.Li>}
    </ul>
  );
};

Ul.Li = ({ children }) => (
  <div className="flex flex-row gap-2 items-baseline py-0.5">
    <IoIosDisc size={8} />
    <li className="flex-1 text-textPrimary tracking-wide">
      {children}
    </li>
  </div>
);

const Ol = (props) => {
  const { children, ...rest } = props;
  return (
    <ol className="list-none list-inside px-4 text-gray-500" {...rest}>
      {children instanceof Array && children.filter(child => typeof child != "string").map(
        // eslint-disable-next-line react/no-array-index-key
        (child, idx) => <Ol.Li key={idx} idx={idx} {...child.props} />,
      )}
      {!(children instanceof Array) && <Ol.Li>{children}</Ol.Li>}
    </ol>
  );
};

Ol.Li = (props) => {
  const { children, idx } = props;
  return (
    <div className="flex flex-row gap-2 items-baseline py-0.5">
      <div>
        {idx + 1}
        {' '}
        .
      </div>
      <li className="flex-1 text-textPrimary tracking-wide">
        {children}
      </li>
    </div>
  );
};

// eslint-disable-next-line jsx-a11y/anchor-has-content
const A = (props) => <a className="text-indigo-400 font-medium" {...props} />;

// eslint-disable-next-line import/prefer-default-export
export const MDXComponents = {
  p: (props) => <div className="text-textPrimary tracking-wide leading-6 " {...props} />,
  h1: (props) => <div className="text-textPrimary text-3xl font-bold text-center" {...props} />,
  h2: (props) => <div className="text-textPrimary text-2xl font-semibold tracking-thin underline underline-offset-4 decoration-wavy" {...props} />,
  h3: (props) => <div className="text-textPrimary text-xl font-medium py-2 tracking-wide underline underline-offset-4 decoration-wavy" {...props} />,
  img: (props) => (
    <div className="w-full flex flex-row justify-center bg-gray-200 object-cover">
      <img {...props} alt="This is an img" className="w-full" />
    </div>
  ),
  a: A,
  hr: () => <div className="bg-gray-700 h-0.5 mb-2 mt-1" />,
  code: (props) => <CodeBlock className="font-code" {...props} />,
  blockquote: (props) => <blockquote className="bg-darkHeader px-4 py-2" {...props} />,
  ul: Ul,
  ol: Ol,
};
