import Highlight, {Prism} from 'prism-react-renderer';
import oceanicNext from 'prism-react-renderer/themes/oceanicNext';

const ThemeWrapper = ({language, code, ...props}) => {
  return <Highlight
    theme={oceanicNext}
    Prism={Prism}
    code={code}
    language={language}
    {...props}
  />
}

const Block = ({className: className2, style, tokens, getLineProps, getTokenProps}) => {
  if (tokens && tokens.length === 1) {
    // console.log("tokens: ", tokens[0], getLineProps({line: tokens[0], key: 0}))
    return <span className="bg-code-inline px-1.5 py-0.5 font-code">{tokens[0][0].content}</span>
  }

  return <pre
    className={`${className2} font-code rounded-lg bg-gray-200 overflow-x-auto text-xs sm:text-base`}
    style={{...style, padding: '20px'}}
  >
    {tokens.map((line, i) => {
      return (
        // eslint-disable-next-line react/no-array-index-key
        <div key={i} {...getLineProps({line, key: i})}>
          {line
            .filter((token) => !token.empty)
            .map((token, key) => (
              // eslint-disable-next-line react/no-array-index-key
              <span key={key} {...getTokenProps({token, key})} />
            ))}
        </div>
      )
    })}
  </pre>
}

const CodeBlock = ({children, className}) => {
  const language = className.replace(/language-/, '');
  return <ThemeWrapper language={language} code={children}>
    {props => <Block {...props}  />}
  </ThemeWrapper>
}

const CodeBlock2 = ({children, className}) => {
  const language = className.replace(/language-/, '');
  return (
    <Highlight
      theme={oceanicNext}
      Prism={Prism}
      code={children}
      language={language}
    >
      {({
          className: className2,
          style,
          tokens,
          getLineProps,
          getTokenProps,
        }) => (
        <pre
          className={`${className2} font-code rounded-lg bg-gray-200 overflow-x-auto text-xs sm:text-base`}
          style={{...style, padding: '20px'}}
        >
          {tokens.map((line, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={i} {...getLineProps({line, key: i})}>
              {line
                .filter((token) => !token.empty)
                .map((token, key) => (
                  // eslint-disable-next-line react/no-array-index-key
                  <span key={key} {...getTokenProps({token, key})} />
                ))}
            </div>
          ))}
        </pre>
      )}
    </Highlight>
  );
};

export default CodeBlock;
