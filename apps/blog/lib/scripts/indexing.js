import fsPromise from 'fs/promises';
import blogConfig from '../../blogs.config';
import { listBlogs } from '../blogs';

const exists = async (itemPath) => {
  try {
    await fsPromise.stat(itemPath);
    return true;
  } catch (err) {
    return false;
  }
};

(async () => {
  if (!await exists(blogConfig.blogsDir)) {
    await fsPromise.mkdir(blogConfig.blogsDir, { recursive: true });
  }

  if (!await exists(blogConfig.indexDir)) {
    await fsPromise.mkdir(blogConfig.indexDir, { recursive: true });
  }

  const blogsMetaList = await listBlogs();
  blogsMetaList.sort((x, y) => {
    const i = Date.parse(x.date) / 1e4;
    const j = Date.parse(y.date) / 1e4;
    return i > j ? -1 : 1;
  });

  const blogsMetaMap = blogsMetaList.reduce(
    (acc, curr) => ({ ...acc, [curr.id]: curr }),
    {},
  );

  const blogsIdList = blogsMetaList.map((item) => item.id);

  await Promise.all([
    fsPromise.writeFile(blogConfig.metadataMap, JSON.stringify(blogsMetaMap, null, 2)),
    fsPromise.writeFile(blogConfig.metaDataIdList, JSON.stringify(blogsIdList, null, 2)),
  ]);
})();
