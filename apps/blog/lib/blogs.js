import fsPromise from 'fs/promises';
import blogConfig from '../blogs.config';
import fm from 'front-matter';
import path from 'path';

export const extractMetaFromBlog = async (blogFile) => {
  const data = await fsPromise.readFile(blogFile);
  const meta = fm(data.toString()).attributes;
  return {
    ...meta,
    tags: (meta.tags || '').split(',').map((item) => item.trim()),
    path: blogFile,
    id: Buffer.from(blogFile).toString('base64'),
  };
};

export const listBlogs = async () => {
  const blogsList = await fsPromise.readdir(blogConfig.blogsDir);
  const p = blogsList
    .filter((item) => item.endsWith('.md') || item.endsWith('.mdx'))
    .map(async (blog) => extractMetaFromBlog(path.resolve(blogConfig.blogsDir, blog)));
  return Promise.all(p);
};
