const blogRoutes = {
  HOME: '/home',
  BLOG: '/blog',
  ABOUT: '/about',
  CONTACT: '/contact',
};

export default blogRoutes;
